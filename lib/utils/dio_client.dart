
import 'dart:convert';

import 'package:dio/dio.dart';

import '../model/books.dart';


class DioClient {


  static final iBookInstance = Dio(BaseOptions(
    baseUrl: 'https://api.itbook.store/1.0/',
    connectTimeout: const Duration(seconds: 5),
    receiveTimeout: const Duration(seconds: 3),
    contentType: 'application/json'
  ));

  static Future<Response> iBookGet({required String path, Map<String,dynamic>? queryString}) async {
    try {
      var response =  await iBookInstance.request(
                    path,
                    queryParameters: queryString ,
                    options: Options(method: 'GET'),
                  );
      return response;
    }
     catch (e) {
      print('error: $e'); 
      rethrow;
      // if (onFailed != null){
      //   onFailed(e);
      // }
    }
  }

  static Future<Response>  iBookPost({required String path, Map<String,dynamic>? bodyData}) async {
    try {
     var response =  await iBookInstance.request(
                    path,
                    data: bodyData,
                    options: Options(method: 'POST'),
                  );
      return response;
    }
     catch (e) {
      rethrow;

    }
  }

}


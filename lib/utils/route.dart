import 'package:flutter/material.dart';
import 'package:shoten/model/book.dart';
import 'package:shoten/screen/init_screen.dart';

import '../screen/book_detail_screen.dart';

class AppRoute {
  
  static String booksScreen="/";
  static String detailBookScreen="/detail_book";

  // static Map<String, WidgetBuilder> myRoutes(context) {
  //   return {
  //     booksScreen: (context) => InitScreen(),
  //     detailBookScreen: (context) => BookDetailScreen(),
  //    };
  // }

  static onGenerateRoutes (RouteSettings settings) {
        print('build route for ${settings.name}');
        var routes = <String, WidgetBuilder>{
          AppRoute.detailBookScreen: (ctx) => BookDetailScreen(settings.arguments as Book),
          AppRoute.booksScreen: (ctx) => InitScreen(),
        };
        WidgetBuilder? builder = routes[settings.name];
        return MaterialPageRoute(builder: (ctx) => builder!(ctx));
      }

}
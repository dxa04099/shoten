import 'package:shoten/model/book_detail.dart';
import 'package:shoten/utils/dio_client.dart';

import '../model/books.dart';

class ApiService {

    //API path iBookInstance:
  static const String ALL_NEW_BOOK = "new";
  static const String DETAIL_BOOK = "books";
  static const String SEARCH_BOOK = "search";


 static Future<Books> getNewBOOK() async {
  try {
     final response = await DioClient.iBookGet(path: ApiService.ALL_NEW_BOOK);
     final Books books = Books.fromJson(response.data);
     return books;
  } catch (e) {
      // print(e); 
    rethrow;
  }
 }

 static Future<BookDetail> getBookDetail({required String bookId}) async {
  try {
     final response = await DioClient.iBookGet(path: "${ApiService.DETAIL_BOOK}/$bookId");
     final BookDetail bookDetail = BookDetail.fromJson(response.data);
     return bookDetail;
  } catch (e) {
      // print(e); 
    rethrow;
  }
 }
 
 static Future<Books> getBookSearch({required String bookName}) async {
  try {
     final response = await DioClient.iBookGet(path: "${ApiService.SEARCH_BOOK}/$bookName");
     final Books bookDetail = Books.fromJson(response.data);
     return bookDetail;
  } catch (e) {
      // print(e); 
    rethrow;
  }
 }

 
}
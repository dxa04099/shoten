import 'package:flutter/material.dart';

class NavigationUtils {
  static List<String>? routeMap;

  static push({required BuildContext context, required String target,dynamic navArguments = null}){
    routeMap?.add(target);
    if(navArguments != null){
      Navigator.of(context).pushNamed(target,arguments: navArguments) ;
    }else {
      Navigator.of(context).pushNamed(target) ;
    }
    
  }

  static pop(BuildContext context){
    Navigator.of(context).pop() ;
    routeMap?.removeLast();
  }

  static popto(BuildContext context,String target){
    while (routeMap?[routeMap!.length]!= target){
      routeMap?.removeLast();
    }
    Navigator.of(context).popUntil(ModalRoute.withName(target)) ;
  }

  static String getCurrentRoot() {
    if (routeMap == null){
      return "N/A";
    }else{
      return routeMap![routeMap!.length-1];
    }
  }

  static List<String>? getRoute() {
    return routeMap;
  }

}
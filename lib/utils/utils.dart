
import 'package:flutter/material.dart';

class Utils{
  
  static ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showSnackbar(BuildContext context,String text){
    return  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        duration: const Duration(seconds: 1),
                        content: Text(text),
                      )
                    );
  } 
}
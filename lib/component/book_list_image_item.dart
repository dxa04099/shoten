
import 'package:flutter/material.dart';

import '../model/book.dart';
import '../utils/navigation_utils.dart';
import '../utils/route.dart';

class BookListImageItem extends StatelessWidget{

  final Book book;

  const BookListImageItem(this.book, {super.key});
  
  @override
  Widget build(BuildContext context) {
    final widthBody = MediaQuery.of(context).size.width;
    final heightBody = MediaQuery.of(context).size.height;

    return InkWell(
      child:  Container(
                height: heightBody * 0.3 ,
                width: widthBody * 0.35 ,
                child: Image.network(book.image,
                  fit: BoxFit.cover,
                )
              ),
              onTap: () {
                NavigationUtils.push(context: context,target: AppRoute.detailBookScreen,navArguments: book);
                }
    );
  }

}
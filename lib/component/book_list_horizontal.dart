
import 'package:flutter/material.dart';

import '../model/books.dart';
import 'book_list_image_item.dart';

class BookListHorizontal extends StatelessWidget {
  const BookListHorizontal({
    Key? key,
    required this.heightBody,
    required this.booksSearchResult,
  }) : super(key: key);

  final double heightBody;
  final Future<Books> booksSearchResult;

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.amber,
      height: heightBody* 0.25,
      child:    FutureBuilder<Books>(
        future: booksSearchResult,
        builder: (context, snapshots) {
          if(snapshots.hasData){
            return 
            ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: snapshots.data!.books.length ,
              itemBuilder: (context, index) {
                 return BookListImageItem(snapshots.data!.books[index]);
                 }
              );
            ;
          }
          else{
            return const Center(child: CircularProgressIndicator());
          }
        }
      
      ),
    );
  }
}
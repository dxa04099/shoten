
import 'package:flutter/material.dart';

import '../model/book_detail.dart';

class TopBookDetail extends StatelessWidget {
  
  final BookDetail bookDetail;

  const TopBookDetail({
    Key? key,
    required this.bookDetail,
  }) : super(key: key);

  

  
  @override
  Widget build(BuildContext context) {
    final widthBody = MediaQuery.of(context).size.width;
    final heightBody = MediaQuery.of(context).size.height;

    return Container(
      width: widthBody ,
      height: heightBody * 0.2,
      // padding: EdgeInsets.all(3),
      // color: Colors.amber,
      child: Row(
        children: [
          Image.network(bookDetail.image,
                  fit: BoxFit.cover,
                  height: heightBody *0.3,
                ),
          Container(
            width: widthBody *0.6,
            // color: Colors.blueAccent,
            // height: heightBody *0.3,
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(bookDetail.title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                Text(bookDetail.authors),
                const Spacer(),
                Text(bookDetail.subtitle),
                const SizedBox(height: 15),
                Text(bookDetail.price,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.green),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
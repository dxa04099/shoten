import 'package:flutter/material.dart';
import 'package:shoten/utils/navigation_utils.dart';
import 'package:shoten/utils/route.dart';
import '../model/book.dart';

class BooksListImageItem extends StatelessWidget {

  final Book book;
  final Color color;


  const BooksListImageItem(this.book,[this.color = Colors.white]);
  

  // ignore: empty_constructor_bodies
  @override
  Widget build(BuildContext context) {

    String imageSource = book.image ;
    final widthBody = MediaQuery.of(context).size.width;
    final heightBody = MediaQuery.of(context).size.height;

    
    return
     InkWell(
              child:  ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(5)),
                child: Container(
                  color: Colors.white38,
                  height: heightBody *0.2,
                  child: Row(
                    children: [
                      Image.network(imageSource,
                        fit: BoxFit.fill,
                      ),
                      Container(
                        // color: Colors.amber,
                        width: widthBody *0.58,
                        padding: const EdgeInsets.fromLTRB(0, 20, 20, 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(book.title,
                              style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: 10,),
                            Container(
                              height: heightBody * 0.07,
                              child: Text(book.subtitle,
                                overflow: TextOverflow.fade,)
                              ),
                            const Spacer(),
                            Row(
                              children: [
                                const Spacer(),
                                Text(book.price,
                                  style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.brown),
                                ),
                                ]
                            ),
              
                          ],
                        ),
                      ),
                    ]
                  ),
                ),
              ),
              onTap: () {
                NavigationUtils.push(context: context,target: AppRoute.detailBookScreen,navArguments: book);
                }
      ); 
  }
}
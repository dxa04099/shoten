// To parse this JSON data, do
//
//     final books = booksFromJson(jsonString);

import 'dart:convert';

import 'book.dart';

// Books booksFromJson(String str) => Books.fromJson(json.decode(str));

// String booksToJson(Books data) => json.encode(data.toJson());

class Books {
    Books({
        required this.error,
        required this.total,
        required this.books,
    });

    final String error;
    final String total;
    final List<Book> books;

    factory Books.fromJson(Map<String, dynamic> json) => Books(
        error: json["error"],
        total: json["total"],
        books: List<Book>.from(json["books"].map((x) => Book.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "error": error,
        "total": total,
        "books": List<dynamic>.from(books.map((x) => x.toJson())),
    };
}

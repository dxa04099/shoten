import 'package:flutter/material.dart';
import 'package:shoten/component/books_list_item.dart';
import 'package:shoten/model/books.dart';
import 'package:shoten/utils/api_call_service.dart';


class InitScreen extends StatefulWidget{
  const InitScreen({super.key});
 
  @override
  State<StatefulWidget> createState() => _InitScreen();
  
}

class _InitScreen extends State<InitScreen>{
  late Future<Books> booksResult;
  
  @override
  initState() {
    super.initState();
    // print("initState ");
    booksResult = ApiService.getNewBOOK();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.bottomRight,
              colors: [Colors.white, Colors.lightGreen])),
        child: Scaffold(
          appBar: AppBar(title: const Text('shoten'),
            backgroundColor: Colors.lightGreen
          ),
          backgroundColor: Colors.transparent,
          body: FutureBuilder<Books> (
            future: booksResult,
            builder: (context, snapshot) {
              if (snapshot.hasData){
                return _content(snapshot.data!);
              }else{
                 return const Center(child: CircularProgressIndicator());
              }
            },
          )
        )
      );
  }

  Widget _content(Books booksResult ) {
  
    return ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: booksResult.books.length,
                itemBuilder: (context, index) {
                    return BooksListImageItem(booksResult.books[index]);
            }, 
            separatorBuilder: (BuildContext context, int index) => const SizedBox(height: 20,),
      );
  }
}


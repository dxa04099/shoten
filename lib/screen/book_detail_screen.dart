import 'package:flutter/material.dart';
import 'package:shoten/model/book.dart';
import 'package:shoten/model/book_detail.dart';
import 'package:shoten/utils/api_call_service.dart';
import 'package:shoten/utils/utils.dart';

import '../component/book_detail_top.dart';
import '../component/book_list_horizontal.dart';
import '../model/books.dart';
import '../utils/navigation_utils.dart';
import '../utils/route.dart';


class BookDetailScreen extends StatefulWidget{
  
  final Book book;
  
  const BookDetailScreen (this.book, {super.key});

  
  @override
  State<StatefulWidget> createState() => _BookDetailScreen();
}

class _BookDetailScreen extends State<BookDetailScreen>{
  late Future<BookDetail> bookDetail ;
  late Future<Books> booksSearchResult;

@override
  void initState() {
    super.initState();
    // var id = ;
    bookDetail = ApiService.getBookDetail(bookId: widget.book.isbn13);
    booksSearchResult = ApiService.getBookSearch(bookName: widget.book.title);
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(title: Text(widget.book.title)),
      body: 
      FutureBuilder<BookDetail>(
        future: bookDetail,
        builder: (context, snapshot) {
          if(snapshot.hasData){
            return  _content(snapshot.data!);
          }
          else{
            return const Center(child: CircularProgressIndicator());
          }
        }
       
      ),
    );
  }

  Column _content(BookDetail book) {
    final widthBody = MediaQuery.of(context).size.width;
    final heightBody = MediaQuery.of(context).size.height;

    return Column(
        children: [
          TopBookDetail(bookDetail: book),
          ElevatedButton(onPressed: 
            () {
              Utils.showSnackbar(context, 'Buy this book');
              }
            , 
            child: Container(
              width: widthBody* 0.8,
              child: Row(
                children: [
                  Spacer(),
                  Text("BUY"),
                  Spacer()
                ],
              ),
            )
          ),
          Container(
            // color: Colors.amber,
            padding: EdgeInsets.all(25),
            height: heightBody *0.2,
            child: Text(book.desc),
          ),
          TextButton(onPressed: 
          () {
            Utils.showSnackbar(context, 'View More Detail');
            
          }, 
          child: Text("Read More")
          ),
          Divider(),
          Container(
            // color: Colors.amber,
            height: 30,
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Row(children: [
              Text('similiar'),
              Spacer(),
              TextButton(onPressed:  () {
                //  Utils.showSnackbar(context, 'More Books');
                 NavigationUtils.push(context: context,target: AppRoute.booksScreen);
                
                }
              , child: Text('More'))
            ],),
          ),
          BookListHorizontal(heightBody: heightBody, booksSearchResult: booksSearchResult)
        ],
      );
  }

}




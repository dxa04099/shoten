import 'package:flutter/material.dart';
import 'package:shoten/screen/book_detail_screen.dart';
import 'package:shoten/screen/init_screen.dart';
import 'package:shoten/utils/route.dart';

void main() {
  runApp(const AppInit());
}

class AppInit extends StatelessWidget{

  const AppInit();
  
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      title: 'shoten',
      // home: InitScreen(),
      initialRoute: AppRoute.booksScreen,
      // routes: AppRoute.myRoutes(context),
      onGenerateRoute: ((settings) => AppRoute.onGenerateRoutes(settings))
    );
  }

}